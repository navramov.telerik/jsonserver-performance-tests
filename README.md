**INSTRUCTIONS**
1. Open `Run_Tests_And_Generate_Results.bat` in text editor and replace the `{jmeter bin folder}` with the path to your jmeter bit folder
2. Save and run the bat file
3. Open the results generated in the `jmeter\bin\tmp\demo.results\index.html` file

**----- OR -----**
1. Clone the repo
2. Open `results\demo.result\index.html` and review the report
